﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webpage1.aspx.cs" Inherits="Soln_CrossPostBack_Span.Webpage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/span.css" rel="stylesheet" type ="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
    
        <asp:TextBox ID="txtfield" runat="server" CssClass="txt" />
        <asp:Button ID="btnsubmit" runat="server" Text="Submit" PostBackUrl="~/Webpage2.aspx" CssClass="btn btn2" />
    
    </div>
    </form>
</body>
</html>
