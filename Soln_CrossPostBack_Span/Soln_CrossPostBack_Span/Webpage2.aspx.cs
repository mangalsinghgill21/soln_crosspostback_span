﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_CrossPostBack_Span
{
    public partial class Webpage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.PreviousPage != null)
            {
                string strSizes =
               ((TextBox)PreviousPage.FindControl("txtfield")).Text;

               
                span1.InnerText = strSizes;
                span2.InnerText = strSizes;
                span3.InnerText = strSizes;
                span4.InnerText = strSizes;
                span5.InnerHtml= strSizes;
            }
        }
    }
}